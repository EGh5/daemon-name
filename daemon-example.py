#!/usr/bin/env python
# Eugene Gavrish

import sys, time
from daemon import Daemon

"""
This is an example of daemon with user-defined name,
your can view its name using "top" commnand
"""

class MyDaemon(Daemon):
    i2=0
    filestring="/home/eug/dem/daemon/hello"

    def run(self):
        import setproctitle

        setproctitle.setproctitle('hello daemon!')   # make new name for top or ps
        while True:
            with open(self.filestring ,"a") as f:    # do smth as daemon...
              f.write("string %i \n" % self.i2)
              if self.i2 %40 == 0:
                f.write("==========\n")
            time.sleep(1)

if __name__ == "__main__":

    daemon = MyDaemon('/tmp/daemon-example.pid')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            with open(daemon.filestring ,"w") as f:
                f.write("Beginning! \n")
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
